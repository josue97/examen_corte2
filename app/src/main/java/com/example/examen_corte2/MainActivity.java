package com.example.examen_corte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examen_corte2.Database.Producto;
import com.example.examen_corte2.Database.Productos;

public class MainActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;

    private Productos db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnEditar = findViewById(R.id.btnEditar);
        db = new Productos(this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if(validar())
                {
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(getText(txtCodigo)));
                    producto.setNombreProducto(getText(txtNombre));
                    producto.setPreacio(Float.parseFloat(getText(txtPrecio)));
                    producto.setMarca(getText(txtMarca));
                    producto.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
                    if (db.insertProducto(producto) != -1)
                        Toast.makeText(MainActivity.this, "Se agrego correctamente", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(MainActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "No puedes dejar los campos vacios",
                            Toast.LENGTH_SHORT).show();

                }
                limpiar();
                db.close();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ProductoActivity.class);
                startActivity(intent);
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
    }

    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtNombre) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}